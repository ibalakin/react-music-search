export default class Artist {

    constructor(artist = {}) {
        const {id = null, name = null, primaryGenre = null} = artist;
        this.id = id;
        this.name = name;
        this.primaryGenre = primaryGenre;
    }
}
