export default class Album {

    constructor(artist = {}) {
        
        const {
            id = null, 
            artistId = null, 
            artistName = null, 
            albumName = null, 
            albumThumbnail = null, 
            primaryGenre = null, 
            trackCount = null, 
        } = artist;

        let {
            releaseDate = null
        } = artist;

        if(releaseDate) {
            const date = new Date(releaseDate);
            releaseDate = `${date.getMonth() +1}/${date.getDay()}/${date.getFullYear()}`;
        }

        this.id = id
        this.artistId = artistId;
        this.artistName = artistName;
        this.albumName = albumName;
        this.albumThumbnail = albumThumbnail;
        this.primaryGenre = primaryGenre;
        this.trackCount = trackCount;
        this.releaseDate = releaseDate;
    }
}
