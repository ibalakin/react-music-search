var api = require('./creds.json');
//import * as api from './creds.json';// another way of importing.. 

export const apiService = {
    getArtists,
    getAlbums
}

const headers = { 'x-api-key': api.apikey };

async function post(url, body) {
    return fetch(url, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
    })
}

export function getArtists(searchTerm) {
    let searchBody = { searchTerm };
    return post(api.baseurl + 'artists', searchBody);
}

export function getAlbums(artistId) {
    let searchBody = { id: artistId };
    return post(api.baseurl + 'albums', searchBody);
}