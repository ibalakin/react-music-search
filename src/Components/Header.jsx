import React from 'react';
import logo from '../advantage-music-research_logo.png';

const Header = props => {
    return (
        <div className='text-left'>
            <img src={logo} alt="Logo"></img>
        </div>
    );
}

export default Header;