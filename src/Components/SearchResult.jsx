import React from 'react';
import { apiService } from '../Services/musicsearch.service';
import Album from '../Models/Album';
import Artist from '../Models/Artist';
import {Releases} from '.';
import logo from '../logo.svg';
import * as PropTypes from 'prop-types';


class SearchResult extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            albums: []
        }
    }

    componentDidMount() {
        this.doAlbumSearch(this.props.artist.id)
    }

    doAlbumSearch = async id => {
        const results = await apiService.getAlbums(id);
        const data = await results.json();

        const albums = data.albums
            ? data.albums.sort((a, b) => a.releaseDate < b.releaseDate ? 1 : -1).map(element => new Album(element))
            : []

        this.setState({albums});
    }

    render() {

        const releases = this.state.albums.length > 0
            ? <Releases albums={this.state.albums} />
            : null

        const artistImage = this.state.albums.length > 0
            ? this.state.albums[0].albumThumbnail
            : logo;

        return (
            <div className='container result-container text-left'>
                <div className='row'>
                    <div className='col-sm-2'>
                        <img src={artistImage} alt={this.props.artist.name}></img>
                    </div>
                    <div className='col-sm-2 info'>
                        <div>Name: {this.props.artist.name}</div>
                        <div>Alias:</div>
                    </div>
                    <div className='col-sm-5 info'>
                        <div>Type: {this.props.artist.primaryGenre}</div>
                        {releases}
                    </div>
                    <div className='col-sm-2 info'>
                        <div>Begin Date:</div>
                        <div>End Date:</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SearchResult;

SearchResult.propTypes = {
    artist: PropTypes.instanceOf(Artist)
}