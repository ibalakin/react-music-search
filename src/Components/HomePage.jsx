import React from 'react';
import SearchResult from './SearchResult';
import SearchBar from './SearchBar';
import { apiService } from '../Services/musicsearch.service';
import Artist from '../Models/Artist';

class HomePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            artists: [], // Exercise 6
        }

        this.doSearch = this.doSearch.bind(this);
    }

    doSearch = async searchValue => {
        console.log('search', searchValue);

        const results = await apiService.getArtists(searchValue);
        const data = await results.json();

        let artists = [];

        if(data.artists) {
            artists = data.artists.map(element => new Artist(element))
        }

        this.setState({ artists });
    }

    render() {
        // Exercise 6
        let searchResults = this.state.artists.length > 0 
            ? this.state.artists.map(artist => 
                <SearchResult key={artist.id} artist={artist}/>    
            )
            : <div>No Results!</div>

        return (
            <div className="app-body">
                <SearchBar doSearch={this.doSearch} />
                {searchResults}
            </div>
        )
    }
}

export default HomePage;