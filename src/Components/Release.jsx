import React from 'react';

const Release = props => {
    return (
        <div className='row'>
            <div className='col-sm-7'>Title: {props.albumName}</div>
            <div className='col-sm-5'>Date: {props.releaseDate}</div>
        </div>
    );
}

export default Release;