import React from 'react';
import {Release} from '.';


class Releases extends React.Component {

    /////////
    constructor(props) {
        super(props)

        this.state = {
            show: false
        }
    }

    releasesClicked = () => {
        this.setState( state => ({ show: !state.show }));
    }


    ///////
    render() {
        let releaseList = this.props.albums.length > 0
            ? this.props.albums.map((album) => 
                <Release key={album.id} albumName={album.albumName} releaseDate={album.releaseDate}/>
            )
            : <div>None</div>

        return (
            <>
                <div className='releases-label' onClick={this.releasesClicked}>
                    Releases: {this.props.albums.length}
                </div>
               
                {
                    this.state.show
                    ? <div className='releases-block'>{releaseList}</div>
                    : null
                }
            </>
        )
    }

}

export default Releases;