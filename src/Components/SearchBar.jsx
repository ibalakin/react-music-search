import React from 'react';
import * as PropTypes from 'prop-types';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchValue: ''
        };

    }

    searchChanged = e => {
        this.setState({ searchValue: e.target.value })
    }

    handleKeyDown = e => {
        if(e.key === 'Enter') {
            this.props.doSearch(this.state.searchValue);
        };
    }

    render() {
        return (
            <div className="search container">
                <div className='row'>
                    <input className='col-sm-12' onChange={this.searchChanged} value={this.state.searchValue} onKeyDown={this.handleKeyDown}></input>
                </div>
            </div>
        )
    }

    
}

export default SearchBar;

SearchBar.propTypes = {
    doSearch: PropTypes.func
}